Vue.component('cookie-consent', {
    props: {
    texto_ley: String,
    texto_boton_ok: String,
    texto_boton_ko: String,
    texto_link: String,
    link_link: String
},
    data: function () {
    return {
    visible: true,
    aceptado: false,
    noAceptado: false,
    Aceptado: true
}
},
    created: function(){
    this.visible = true;
    if(this.Aceptado === this.getEstado()){
    this.execJsWithCookies();
}
},
    methods: {
    leyAceptada: function(){
    this.estadosLeyAceptada();
    this.setEstadoAceptado();
},
    leyNoAceptada: function(){
    this.estadosLeyNoAceptada();
    this.setEstadoNoAceptado();
},
    estadosLeyAceptada: function(){
    this.aceptado = true;
    this.visible = false;
},
    estadosLeyNoAceptada: function(){
    this.aceptado = false;
    this.visible = false;
},
    getEstado: function(){
    var Control;
    if (typeof (Storage) !== "undefined") {
    Control = localStorage.getItem("lopd");
} else {
    Control = document.cookie.split(';');
    Control = Control[0].split('=')
    Control = Control[1];
}

    if(null == Control){
    Control = this.noAceptado;
}
    else{
    this.estadosLeyAceptada();
}

    return Control;
},
    setEstadoAceptado: function(){
    if (typeof (Storage) !== "undefined") {
    if (localStorage) {
    localStorage.setItem("lopd", this.Aceptado);
} else {
    document.cookie = "lopd="+this.Aceptado;
}
}
    this.execJsWithCookies();
},
    setEstadoNoAceptado: function(){
    if (typeof (Storage) !== "undefined") {
    if (localStorage) {
    localStorage.setItem("lopd", this.noAceptado);
} else {
    document.cookie = "lopd="+this.noAceptado;
}
}
},
    execJsWithCookies(){
    console.log('Cookies aceptadas');
    var scripts = [
        "https://www.googletagmanager.com/gtag/js?id=UA-2226109-1",
        "external.js"
    ];
    scripts.forEach(script => {
        let tag = document.createElement("script");
        tag.setAttribute("src", script);
        tag.async = true;
        document.head.appendChild(tag);
        console.log("---> " + script);
    });
    //Scripts posteriores
    console.log("scripts posteriores");
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());
    gtag('config', 'UA-2226109-1');
    console.log('Termina el proceso ...');
}
},
    template: '<div id="barraaceptacion" v-show="visible">' +
    '        <div class="inner">' +
    '            <p>{{texto_ley}}</p>' +
    '            <button v-on:click="leyAceptada">{{texto_boton_ok}}</button> |' +
    '            <button v-on:click="leyNoAceptada">{{texto_boton_ko}}</button> |' +
    '            <a href="{{link_link}}" target="_blank" class="info">{{texto_link}}</a>' +
    '        </div>' +
    '</div>'
})
